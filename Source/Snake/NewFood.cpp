// Fill out your copyright notice in the Description page of Project Settings.

#include "Math/UnrealMathUtility.h"
#include "NewFood.h"
#include "Food.h"


// Sets default values
ANewFood::ANewFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANewFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ANewFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

