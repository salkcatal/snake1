// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeElement.generated.h"



class UStaticMeshComponent;
class ASnakeTim;
UCLASS()
class SNAKE_API ASnakeElement : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElement();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;
	UPROPERTY()
		ASnakeTim* SnakeOwner;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintNativeEvent)
		void SetFirstElementType();
	void SetFirstElementType_Implementation();

	virtual void Interact (AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBOdyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void ToggleCollision();
};
