// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameModeBase.h"
#include "Food.h"


void ASnakeGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	AddFood();
}


void ASnakeGameModeBase::AddFood()
{
	float Min = 0;
	float Max = 500;
	FVector NewLocation(FMath::RandRange(Min, Max), FMath::RandRange(Min, Max), 30);
	FTransform NewTransform(NewLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(Foods, NewTransform);
	
}