// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

/**
 * 
 */
class AFood;
UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> Foods;

protected:
	virtual void BeginPlay() override;

public:
	
	UFUNCTION(BlueprintCallable)
		void AddFood();
};
