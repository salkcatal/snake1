// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeTim.h"
#include "SnakeElement.h"
#include "Interactable.h"

// Sets default values
ASnakeTim::ASnakeTim()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;

}

// Called when the game starts or when spawned
void ASnakeTim::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeTim::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);  
	Move1();
}

void ASnakeTim::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0,0);
		FTransform NewTransform(NewLocation);
		ASnakeElement* NewSnakeElements = GetWorld()->SpawnActor<ASnakeElement>(SnakeElement, NewTransform);
		NewSnakeElements->SnakeOwner = this;
		int32 ElemIndex= SnakeElements.Add(NewSnakeElements);
		if (ElemIndex == 0)
		{
			NewSnakeElements->SetFirstElementType();
			
		}

	
	}
}

void ASnakeTim::Move(float DeltaTime)
{
	FVector MovementVector;
	float MovementSpeedDelta = MovementSpeed * DeltaTime;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP :
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpeedDelta;
		break;
	}
	AddActorWorldOffset(MovementVector);
}

void ASnakeTim::Move1()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElemenet = SnakeElements[i];
		auto PrevElements = SnakeElements[i-1];
		FVector PrevLocation = PrevElements->GetActorLocation();
		CurrentElemenet->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeTim::SnakeElementOverlap(ASnakeElement* OverplappedElement, AActor* Other)
{
	if (IsValid(OverplappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverplappedElement, ElemIndex);
		bool blsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, blsFirst);
		}
	}
}
