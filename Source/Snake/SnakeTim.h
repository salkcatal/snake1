// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeTim.generated.h"


class ASnakeElement;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeTim : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeTim();
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElement> SnakeElement;
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY()
		TArray<ASnakeElement*> SnakeElements;
	UPROPERTY(EditDefaultsOnly)
		EMovementDirection LastMoveDirection;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum=1);
	UFUNCTION(BlueprintCallable)
	void Move(float DeltaTime); // ������ �������� ��������
	UFUNCTION(BlueprintCallable)
	void Move1();
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElement* OverplappedElement, AActor* Other);
};
